<?php


function qm_invitation_dash_board(){

  $output = l(t('invitation message template'), 'admin/config/qm-invitation/invitation');
  $output .= '<br />';
  $output .= l(t('Post sharing message template'), 'admin/config/qm-invitation/post-share');

  return $output;
}


/**
 * @param $form
 * @param $form_state
 * @return array
 * This form is used for setting the invitation e-mail template by admin.
 */
function qm_invitation_message_admin_form($form, &$form_state) {

  $form['invitation_copy_admin'] = array(
    '#title' => t('Send a copy to admin e-mail:'),
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => variable_get('invitation_copy_admin'),
    '#description' => t('Please enter admin e-mail.'),
  );
  $form['invitation_subject'] = array(
    '#title' => t('e-mail subject:'),
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 300,
    '#default_value' => variable_get('qm_invitation_subject'),
    '#description' => t('Please enter invitation e-mail subject.'),
  );
  $form['invitation_body'] = array(
    '#title' => t('Message body:'),
    '#type' => 'textarea',
    '#rows' => 2,
    '#cols' =>30,
    '#default_value' => variable_get('qm_invitation_body'),
    '#description' => t('Please enter the message.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}

/**
 * @param $form
 * @param $form_state
 * @return void
 */
function qm_invitation_message_admin_form_submit($form, &$form_state){

  GLOBAL $user;

  $result_invitation_subject = $form_state['values']['invitation_subject'];
  $result_invitation_body = $form_state['values']['invitation_body'];
  $result_admin_copy = $form_state['values']['invitation_copy_admin'];

  variable_set('qm_invitation_subject', $result_invitation_subject);
  variable_set('qm_invitation_body', $result_invitation_body);
  variable_set('invitation_copy_admin', $result_admin_copy);
}


/**
 * @param $form
 * @param $form_state
 * @return array
 * This form is used for setting the qorum post sharing e-mail template by admin.
 */
function qm_invitation_share_admin_form($form, &$form_state) {

  $form['post_share_subject'] = array(
    '#title' => t('e-mail subject:'),
    '#type' => 'textfield',
    '#size' => 40,
    '#maxlength' => 300,
    '#default_value' => variable_get('qm_post_share_subject'),
    '#description' => t('Please enter post sharing e-mail subject.'),
  );
  $form['post_share_body'] = array(
    '#title' => t('Message body:'),
    '#type' => 'textarea',
    '#rows' => 2,
    '#cols' =>30,
    '#default_value' => variable_get('qm_post_share_body'),
    '#description' => t('Please enter the message.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit')
  );

  return $form;
}


function qm_invitation_share_admin_form_submit($form, &$form_state){

  GLOBAL $user;

  $result_share_subject = $form_state['values']['post_share_subject'];
  $result_share_body = $form_state['values']['post_share_body'];

  variable_set('qm_post_share_subject', $result_share_subject);
  variable_set('qm_post_share_body', $result_share_body);
}


