jQuery(document).ready(function() {

//    console.log('ready');
  jQuery('a.qm-invitation-invite').click(qorum_invitation_modal);
  jQuery('a.qm-invitation-share').click(qorum_invitation_modal);

});



function qorum_invitation_modal(){
//    console.log('click');
  var dialogOpts = {
    modal: true
  };

  jQuery.ajax({
    url: this.href,
    type: "POST",
    success: function(data){
//                console.log('success');
      jQuery(function(){
        jQuery("div.qm-invite-modal").html(data);

      });
    },
    data: 'js=1' // Pass a key/value pair.


  });
  jQuery(".qm-invite-modal").dialog(dialogOpts);

  return false;
}
