<?php
/**
 * Created by JetBrains PhpStorm.
 * User: root
 * Date: 4/5/12
 * Time: 2:41 PM
 * To change this template use File | Settings | File Templates.
 */


function qorum_dashboard(){
  $output = "";

  return $output;
}



/**
 * @param $cid
 * @return 
 */

function qorum_comment_revisions($cid){

  global $user;

  //Change the title by drupal built in function.
  drupal_set_title('Comment Revisions');


  //Select needed data from comment_revision table that will show all revision there for individual commnet id(cid),
  $comment_revisions_log = db_select('comment_revision', 'c')
    ->fields('c', array('cid','uid','revision_id','changed','name' ))
    ->condition('cid', $cid)
    ->orderBy('revision_id','DESC')
    ->execute()->fetchAll();

  //Find revision_id from comment_comment_revisions_assign table for compare between revision_id of comment_revision table to revision_id of comment_comment_revisions_assign.
  $comment_revisions_id = db_select('comment_comment_revisions_assign', 'ccra')
    ->fields('ccra', array('revision_id' ))
    ->condition('cid', $cid)
    ->execute()->fetchObject();


  //If $comment_revisions_log is empty (from comment_revision table) then won't show revision log.
  if(!empty($comment_revisions_log)){

    //Data fetch from comment_revision table.
    foreach($comment_revisions_log as $rev_log){

      $rev_id[] = $rev_log->revision_id;

      //Compare revision_id for show the current revision.
      if($rev_log->revision_id == $comment_revisions_id->revision_id){

        $items[] = array(
          'name' => l(date('j M Y - h:ia', $rev_log->changed),'comment/'.$rev_log->cid.'/revisions/'.$rev_log->revision_id.'/view').' by '.l($rev_log->name, 'users/'.$rev_log->name),
          'revert' => '<i>current version</i>',
          'delete' => '',
        );

      }else{
        $items[] = array(
          'name' => l(date('j M Y - h:ia', $rev_log->changed),'comment/'.$rev_log->cid.'/revisions/'.$rev_log->revision_id.'/view').' by '.l($rev_log->name, 'users/'.$rev_log->name),
          'revert' => l('revert','comment/'.$rev_log->cid.'/revisions/'.$rev_log->revision_id.'/revert'),
          'delete' => l('delete','comment/'.$rev_log->cid.'/revisions/'.$rev_log->revision_id.'/delete'),
        );
      }



    }

    //Header create for drupal theme().
    $header = array('REVISION', 'OPERATIONS', '' );

    //revision log show by drupal default theme() functionss.
    $output = theme('table', array(
                                  'header' => $header,
                                  'rows' => $items,
                             ));
  }else{

    $items[] = array(
          'name' => "There are no revision here.",
        );

    $header = array('REVISION');
    $output = theme('table', array(
                                  'header' => $header,
                                  'rows' => $items,
                             ));
  }



  return $output;
}



/**
 * @param $cid
 * @param $revision_id
 * @return string
 */

function comment_revision_details($cid, $revision_id){

  //Select data from comment_revision table for create dynamically Revision Title.
  $comment_revisions_log = db_select('comment_revision', 'c')
    ->fields('c', array('cid','uid','revision_id','changed','subject','name' ))
    ->condition('revision_id', $revision_id)
    ->execute()->fetchObject();


  //Set title by drupal default drupal_set_title() function.
  drupal_set_title('Revision of '.$comment_revisions_log->subject.' from '.date('j M Y - h:ia', $comment_revisions_log->changed));

  //Go To revisions_info() for brought revision_log information.
  $output = revisions_info($cid, $revision_id);


  return $output;
}



/**
 * @param $cid
 * @param $revision_id
 * @return string
 */

function revisions_info($cid, $revision_id){

  //Data fetch from comment_revision table and field_revision_comment_body table for show individual revision log.
  $query = db_select('comment_revision', 'cr')
    ->fields('cr', array('uid','subject','created', 'name'));
  $query->join('field_revision_comment_body', 'frcd', 'frcd.revision_id = cr.revision_id');
  $query->fields('frcd',array('revision_id', 'comment_body_value'));
  $query->condition('frcd.revision_id', $revision_id);

  $result = $query->execute()->fetchObject();

  $output = "<div id='comments' class='comment-wrapper'><div class='comment comment-by-viewer clearfix'>";
  $output .= "<div class='comment attribution'>";
  $output .= "<div class='submitted'>";
  $output .= "<p class='commenter-name'>".l($result->name, 'users/'.$result->name)."</p>";
  $output .= "<p class='comment-time'>".(date('j M Y - h:ia', $result->created))."</p>";
  $output .= "</div></div>";

  $output .= "<div class='comment-text'>";
  $output .= "<h3>".$result->subject."</h3>";
  $output .= "<div class='content'>".$result->comment_body_value."</div>";
  $output .= "</div>";

  $output .= "</div></div>";



  return $output;
}




/**
 * @param $cid
 * @param $revision_id
 * @return string
 */

function comment_revision_revert($cid, $revision_id) {

  //When click revert for change comment from old revision log then updated revision_id into comment_comment_revision_assign table.
  $comment_comment_revisions_assign = db_update('comment_comment_revisions_assign')
    ->fields(array(
                  'revision_id' => $revision_id,
             ))
    ->condition('cid', $cid)
    ->execute();


  //Find some data from comment_revision table for update comment table.
  $comment_revision = db_select('comment_revision', 'c')
    ->fields('c', array('cid','pid','revision_id','subject','changed','thread'))
    ->condition('revision_id', $revision_id)
    ->execute()->fetchObject();

  //Update comment table according to comment_revision table query's information.
  $comment = db_update('comment')
    ->fields(array(
               'pid' => $comment_revision->pid,
               'subject' => "$comment_revision->subject",
               'changed' => $comment_revision->changed,
               'thread' => $comment_revision->thread,
             ))
    ->condition('cid',$cid)
    ->execute();


  $body_rev = db_select('field_revision_comment_body', 'frcb')
    ->fields('frcb', array('entity_id','revision_id','comment_body_value', 'comment_body_format'))
    ->condition('revision_id', $revision_id)
    ->execute()->fetchObject();

  $body = filter_xss($body_rev->comment_body_value);



  //Update field_data_comment_body table according to above field_revision_comment_body table query's information.
  $body_data = db_update('field_data_comment_body')
    ->fields(array(
               'revision_id' => $body_rev->revision_id,
               'comment_body_value' => $body,
               'comment_body_format' => $body_rev->comment_body_format,
             ))
    ->condition('entity_id', $cid)
    ->execute();



  //When run revert function then clear cache automatically.
  //  $core = array('cache', 'cache_path', 'cache_filter', 'cache_bootstrap', 'cache_page');
  $core = array('cache');
  $cache_tables = array_merge(module_invoke_all('flush_caches'), $core);
  foreach ($cache_tables as $table) {
    cache_clear_all('*', $table, TRUE);
  }


  //After finished this function operation then redirect to below path automatically. Here used drupal built in drupa_goto() function.
  $path = "comment/".$cid."/revisions";
  drupal_goto($path);
  return "Hello!";

}



/**
 * @param $cid
 * @param $revision_id
 * @return string
 */

function comment_revision_delete($cid, $revision_id) {

  //When click delete from revision log then deleted a row from comment_revision according to selected revision_id.
  $comment_rev = db_delete('comment_revision')
    ->condition('revision_id', $revision_id)
    ->execute();

  //Deleted a row from field_revision_comment_body table.
  $body_rev = db_delete('field_revision_comment_body')
    ->condition('revision_id', $revision_id)
    ->execute();


  //After finished this function operation then redirect to below path automatically. Here used drupal built in drupa_goto() function.
  $path = "comment/".$cid."/revisions";
  drupal_goto($path);
  return "Hello!";

}